var lletres = ['T', 'R', 'W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E','T'];

var dni = prompt("Introdueix només el número de DNI: ");
var lletra = prompt("Introdueix la lletra del DNI: ");
var lletra1 = lletra.toUpperCase();
var dniSep = dni.split("");
var dniNum = parseInt(dni);
if (dniSep.length != 8) {
    console.log("No té 8 números, el DNI no és vàlid!");
}else{
    if (dniNum < 0 || dniNum > 99999999){
        console.log("El número proporcionat no és vàlid!");
    }else{
        var posicio = dniNum % 23;
        if (lletres[posicio] == lletra1){
            console.log("El número i la lletra del DNI són correctes");
        }else{
            console.log("La lletra del DNI no és correcta");
        }
    }
}
