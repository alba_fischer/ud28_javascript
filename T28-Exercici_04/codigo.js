var valors = [true, 5, false,"hola","adios",2];

//Part 1
if (valors[3].length < valors[4].length){
    console.log(valors[4] + " tiene más carácteres (es mayor) que " + valors[3]);
}else{
    console.log(valors[3] + " tiene más carácteres (es mayor) que " + valors[4]);
}

//Part 2
console.log("Cas operador or:");
if (valors[0] || valors[2]){
    console.log(true);
}else{
    console.log(false);
}
console.log("Cas operador and:");
if (valors[0] && valors[2]){
    console.log(true);
}else{
    console.log(false);
}
//Part 3
//suma
console.log(valors[1] + " + " + valors[5] + "= " + (valors[1] + valors[5]));

//resta
console.log(valors[1] + " - " + valors[5] + "= " + (valors[1] - valors[5]));

//multiplicació
console.log(valors[1] + " * " + valors[5] + "= " + (valors[1] * valors[5]));

//divisió
console.log(valors[1] + " / " + valors[5] + "= " + (valors[1] / valors[5]));

//residu
console.log(valors[1] + " % " + valors[5] + "= " + (valors[1] % valors[5]));


